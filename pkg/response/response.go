package response

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/errors"
)

func NewResponse(
	ctx *gin.Context,
	data interface{},
	err error,
) {
	ctx.JSON(errors.GetStatus(err), gin.H{
		"code":    errors.GetCode(err),
		"message": errors.GetMessage(err),
		"data":    data,
		"errors":  errors.GetError(err),
	})
}
