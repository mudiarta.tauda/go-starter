package code

import "net/http"

type Code uint16

const (
	SUCCESS           Code = 10
	UNKNOWN           Code = 20
	PERMISSION_DENIED Code = 30
	INVALID_INPUT     Code = 40
	NOT_FOUND         Code = 50
	INTERNAL          Code = 60
)

func Status(code Code) int {
	switch {
	case code == SUCCESS:
		return http.StatusOK
	case code == PERMISSION_DENIED:
		return http.StatusUnauthorized
	case code == INVALID_INPUT:
		return http.StatusBadRequest
	case code == NOT_FOUND:
		return http.StatusNotFound
	default:
		return http.StatusInternalServerError
	}
}
