package errors

import (
	"fmt"

	"gitlab.com/mudiarta.tauda/go-starter/pkg/errors/code"
)

type errors struct {
	Code    code.Code
	Message string
	Err     error
}

func New(code code.Code, message string, err error) *errors {
	return &errors{Code: code, Message: message, Err: err}
}
func (e *errors) Error() string {
	return fmt.Sprintf("%d\n%s\n%s", e.Code, e.Err.Error(), e.Message)
}
func GetCode(err error) code.Code {
	if err == nil {
		return code.SUCCESS
	}
	v, ok := err.(*errors)
	if !ok || v == nil {
		return code.UNKNOWN
	}
	return v.Code
}
func GetMessage(err error) string {
	if err == nil {
		return "OK"
	}
	v, ok := err.(*errors)
	if !ok || v == nil {
		return "Unknown Error"
	}
	return v.Message
}
func GetError(err error) *string {
	if err == nil {
		return nil
	}
	merr := err.Error()
	return &merr
}
func GetStatus(err error) int {
	return code.Status(GetCode(err))
}
