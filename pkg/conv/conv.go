package conv

import (
	"strconv"
	"strings"
)

func StrToInt(str string, defaultValue int) int {
	if v, err := strconv.Atoi(strings.TrimSpace(str)); err == nil {
		return v
	}
	return defaultValue

}
