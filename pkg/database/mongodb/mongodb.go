package mongodb

import (
	"context"
	"os"
	"sync"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var (
	client *mongo.Client
	once   sync.Once
)

// get mongodb client
func Client(ctx context.Context) (*mongo.Client, error) {
	var err error
	once.Do(func() {
		ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
		defer cancel()
		clientOptions := options.Client().ApplyURI(os.Getenv("MONGO_HOST")).SetAuth(options.Credential{
			Username: os.Getenv("MONGO_USERNAME"),
			Password: os.Getenv("MONGO_PASSWORD"),
		})
		client, err = mongo.Connect(ctx, clientOptions)
		if err != nil {
			return
		}
		if err = client.Ping(ctx, readpref.Primary()); err != nil {
			return
		}
	})
	return client, err
}

func Shutdown(ctx context.Context) error {
	if client != nil {
		return client.Disconnect(ctx)
	}
	return nil
}
