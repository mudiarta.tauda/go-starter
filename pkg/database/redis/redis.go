package redis

import (
	"os"
	"sync"

	"github.com/redis/go-redis/v9"
)

var (
	client *redis.Client
	once   sync.Once
)

// get redis client
func Client() *redis.Client {
	once.Do(func() {
		client = redis.NewClient(&redis.Options{
			Addr:     os.Getenv("REDIS_HOST"),
			Password: os.Getenv("REDIS_PASSWORD"),
		})
	})
	return client
}

func Shutdown() error {
	if client != nil {
		return client.Close()
	}
	return nil
}
