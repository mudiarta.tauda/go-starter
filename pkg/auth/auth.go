package auth

import (
	"encoding/base64"
	"encoding/json"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/auth/roles"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/errors/code"
)

type JwtUser struct {
	SessionId string      `json:"sessionId"`
	UserId    string      `json:"userId"`
	Roles     roles.Roles `json:"roles"`
}

func decodedJwtUser(str string) (*JwtUser, error) {
	b64, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		return nil, err
	}
	var user JwtUser
	if err := json.Unmarshal(b64, &user); err != nil {
		return nil, err
	}
	return &user, nil
}

type Authorization interface {
	// Jwt
	Jwt(roles ...roles.Roles) gin.HandlerFunc
}

type authorization struct{}

func NewAuthorization() Authorization {
	return &authorization{}
}
func (a *authorization) Jwt(roles ...roles.Roles) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ltoken := strings.Split(ctx.GetHeader("Authorization"), " ")
		if len(ltoken) != 2 {
			ctx.AbortWithStatusJSON(401, gin.H{"code": code.PERMISSION_DENIED, "message": "authorization required", "data": nil, "errors": "authorization required"})
			return
		}
		if ltoken[0] != "Bearer" || ltoken[1] == "" {
			ctx.AbortWithStatusJSON(401, gin.H{"code": code.PERMISSION_DENIED, "message": "authorization required", "data": nil, "errors": "authorization required"})
			return
		}
		jwtUser, err := decodedJwtUser(ltoken[1])
		if err != nil {
			ctx.AbortWithStatusJSON(401, gin.H{"code": code.PERMISSION_DENIED, "message": "invalid token", "data": nil, "errors": "invalid token"})
			return
		}
		if len(roles) == 0 {
			ctx.Set("user", jwtUser)
			ctx.Next()
			return
		}
		for _, r := range roles {
			if jwtUser.Roles == r {
				ctx.Set("user", jwtUser)
				ctx.Next()
				return
			}
		}
		ctx.AbortWithStatusJSON(401, gin.H{"code": code.PERMISSION_DENIED, "message": "permission denied", "data": nil, "errors": "permission denied"})
		return

	}
}
func GetUser(ctx *gin.Context) *JwtUser {
	if v, ok := ctx.Get("user"); v != nil && ok {
		if u, ok := v.(*JwtUser); u != nil && ok {
			return u
		}
	}
	return nil
}
