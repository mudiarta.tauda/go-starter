package roles

type Roles int

const (
	uNKNOWN Roles = 0
	ADMIN   Roles = 10
)
