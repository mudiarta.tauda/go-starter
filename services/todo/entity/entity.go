package entity

import (
	"fmt"
	"time"

	"gitlab.com/mudiarta.tauda/go-starter/pkg/validate"
)

type Todo struct {
	Id          int       `json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedAt"`
}

type Save struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

func (s *Save) Validate() error {
	switch {
	case validate.IsEmpty(s.Name):
		return fmt.Errorf("name required")
	case validate.IsEmpty(s.Description):
		return fmt.Errorf("description required")
	default:
		return nil
	}

}
