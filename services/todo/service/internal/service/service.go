package service

import (
	"context"

	"gitlab.com/mudiarta.tauda/go-starter/pkg/conv"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/errors"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/errors/code"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/entity"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/repository"
)

type Service struct {
	repoTodo repository.Repository
}

func NewService(repoTodo repository.Repository) *Service {
	return &Service{repoTodo: repoTodo}
}

// Get All
func (service *Service) GetAll(ctx context.Context, search string) ([]entity.Todo, error) {
	result, err := service.repoTodo.GetAll(ctx, search)
	if err != nil {
		return nil, errors.New(code.INTERNAL, "failed get todo list", err)
	}
	return result, nil
}

// Create
func (service *Service) Create(ctx context.Context, data entity.Save) (int, error) {
	if err := data.Validate(); err != nil {
		return 0, errors.New(code.INVALID_INPUT, err.Error(), err)
	}
	id, err := service.repoTodo.Create(ctx, data)
	if err != nil {
		return 0, errors.New(code.INTERNAL, "failed create todo", err)
	}
	return id, nil
}

// Get By Id
func (service *Service) GetById(ctx context.Context, id string) (*entity.Todo, error) {
	result, err := service.repoTodo.GetById(ctx, conv.StrToInt(id, 0))
	if err != nil {
		return nil, errors.New(code.NOT_FOUND, "failed find todo by id", err)
	}
	return result, nil
}

// Update
func (service *Service) Update(ctx context.Context, id string, data entity.Save) (int, error) {
	if err := data.Validate(); err != nil {
		return 0, errors.New(code.INVALID_INPUT, err.Error(), err)
	}
	if err := service.repoTodo.Update(ctx, conv.StrToInt(id, 0), data); err != nil {
		return 0, errors.New(code.INTERNAL, "failed update todo", err)
	}
	return conv.StrToInt(id, 0), nil
}

// Delete
func (service *Service) Delete(ctx context.Context, id string) (int, error) {
	if err := service.repoTodo.Delete(ctx, conv.StrToInt(id, 0)); err != nil {
		return 0, errors.New(code.INTERNAL, "failed delete todo", err)
	}
	return conv.StrToInt(id, 0), nil
}

// Delete all
func (service *Service) DeleteAll(ctx context.Context) error {
	items, err := service.repoTodo.GetAll(ctx, "")
	if err != nil {
		return errors.New(code.INTERNAL, "failed get todo list", err)
	}
	for _, it := range items {
		if err := service.repoTodo.Delete(ctx, it.Id); err != nil {
			return errors.New(code.INTERNAL, "failed delete items", err)
		}
	}
	return nil
}
