package service

import (
	"context"

	"gitlab.com/mudiarta.tauda/go-starter/services/todo/entity"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/repository"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/service/internal/service"
)

type Service interface {
	// Get All
	GetAll(ctx context.Context, search string) ([]entity.Todo, error)

	// Create
	Create(ctx context.Context, data entity.Save) (int, error)

	// Get By Id
	GetById(ctx context.Context, id string) (*entity.Todo, error)

	// Update
	Update(ctx context.Context, id string, data entity.Save) (int, error)

	// Delete
	Delete(ctx context.Context, id string) (int, error)

	// Delete all
	DeleteAll(ctx context.Context) error
}

func NewService(repoTodo repository.Repository) Service {
	return service.NewService(repoTodo)
}
