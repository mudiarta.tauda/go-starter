package repository

import (
	"context"

	"gitlab.com/mudiarta.tauda/go-starter/services/todo/entity"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/repository/internal/repository"
)

type Repository interface {
	// Get All
	GetAll(ctx context.Context, search string) ([]entity.Todo, error)

	// Create
	Create(ctx context.Context, data entity.Save) (int, error)

	// Get By Id
	GetById(ctx context.Context, id int) (*entity.Todo, error)

	// Update
	Update(ctx context.Context, id int, data entity.Save) error

	// Delete
	Delete(ctx context.Context, id int) error
}

func NewRepository() Repository {
	return repository.NewRepository()
}
