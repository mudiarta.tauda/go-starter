package repository

import (
	"context"
	"fmt"
	"strings"
	"time"

	"gitlab.com/mudiarta.tauda/go-starter/services/todo/entity"
)

type Repository struct {
	items []Todo
}

func NewRepository() *Repository {
	return &Repository{
		items: make([]Todo, 0),
	}
}

// Get All
func (repo *Repository) GetAll(ctx context.Context, search string) ([]entity.Todo, error) {
	result := make([]entity.Todo, 0)
	for _, t := range repo.items {
		if search == "" {
			result = append(result, t.ToTodoEntity())
		} else if strings.HasPrefix(strings.ToLower(t.Name), strings.ToLower(search)) {
			result = append(result, t.ToTodoEntity())
		}
	}
	return result, nil
}

// Create
func (repo *Repository) Create(ctx context.Context, data entity.Save) (int, error) {
	id := len(repo.items) + 1
	repo.items = append(repo.items, Todo{
		Id:          id,
		Name:        data.Name,
		Description: data.Description,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	})
	return id, nil

}

// Get By Id
func (repo *Repository) GetById(ctx context.Context, id int) (*entity.Todo, error) {
	if id > 0 && id < len(repo.items) {
		result := repo.items[id].ToTodoEntity()
		return &result, nil
	}
	return nil, fmt.Errorf("failed id not found")
}

// Update
func (repo *Repository) Update(ctx context.Context, id int, data entity.Save) error {
	if id > 0 && id < len(repo.items) {
		repo.items[id].Name = data.Name
		repo.items[id].Description = data.Description
		repo.items[id].UpdatedAt = time.Now()
		return nil
	}
	return fmt.Errorf("failed update, id not found")
}

// Delete
func (repo *Repository) Delete(ctx context.Context, id int) error {
	if id > 0 && id < len(repo.items) {
		repo.items = append(repo.items[:id], repo.items[id+1:]...)
		return nil
	}
	return fmt.Errorf("failed delete, id not found")

}
