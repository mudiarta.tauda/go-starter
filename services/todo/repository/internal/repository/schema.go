package repository

import (
	"time"

	"gitlab.com/mudiarta.tauda/go-starter/services/todo/entity"
)

type Todo struct {
	Id          int
	Name        string
	Description string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

func (t Todo) ToTodoEntity() entity.Todo {
	return entity.Todo{
		Id:          t.Id,
		Name:        t.Name,
		Description: t.Description,
		CreatedAt:   t.CreatedAt,
		UpdatedAt:   t.UpdatedAt,
	}
}
