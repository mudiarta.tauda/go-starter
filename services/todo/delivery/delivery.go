package delivery

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/gin-gonic/gin"
	"github.com/robfig/cron/v3"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/auth"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/delivery/internal/restapi"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/delivery/internal/worker"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/repository"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/service"
)

func NewRestApi(handler *gin.Engine, auth auth.Authorization, service service.Service) {
	restapi.NewRestApi(handler, auth, service)
}

func NewWorkerDeleteAll() error {
	handler := cron.New()
	if err := worker.DeleteAll(handler, service.NewService(repository.NewRepository())); err != nil {
		return err
	}
	handler.Start()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM, syscall.SIGQUIT)
	<-quit
	return nil
}
