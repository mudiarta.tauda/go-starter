package worker

import (
	"context"
	"fmt"
	"os"

	"github.com/robfig/cron/v3"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/service"
)

func DeleteAll(handler *cron.Cron, service service.Service) error {
	fmt.Println(os.Getenv("TODO_WORKER_DELETE_ALL"))
	if os.Getenv("TODO_WORKER_DELETE_ALL") == "" {
		return fmt.Errorf("env TODO_WORKER_DELETE_ALL required")
	}
	handler.AddFunc(os.Getenv("TODO_WORKER_DELETE_ALL"), func() {
		fmt.Println("Start TODO WORKER DELETE ALL")
		if err := service.DeleteAll(context.Background()); err != nil {
			fmt.Printf("Error TODO WORKER DELETE ALL: %s", err.Error())
			return
		}
		fmt.Println("Success TODO WORKER DELETE ALL")
	})
	return nil
}
