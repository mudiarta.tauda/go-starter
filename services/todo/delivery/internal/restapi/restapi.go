package restapi

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/auth"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/auth/roles"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/errors"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/errors/code"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/response"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/entity"
	"gitlab.com/mudiarta.tauda/go-starter/services/todo/service"
)

func NewRestApi(handler *gin.Engine, auth auth.Authorization, service service.Service) {
	route := handler.Group("/todo/v1")
	route.GET("", getAll(service))
	route.POST("", auth.Jwt(roles.ADMIN), create(service))
	route.GET("/:id", getById(service))
	route.PUT("/:id", auth.Jwt(roles.ADMIN), update(service))
	route.DELETE("/:id", auth.Jwt(roles.ADMIN), delete(service))

}
func getAll(service service.Service) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		result, err := service.GetAll(ctx, ctx.Param("search"))
		response.NewResponse(ctx, result, err)
	}
}
func create(service service.Service) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var data entity.Save
		if err := ctx.BindJSON(&data); err != nil {
			response.NewResponse(ctx, nil, errors.New(code.INVALID_INPUT, "invalid requrest body", err))
			return
		}
		result, err := service.Create(ctx, data)
		response.NewResponse(ctx, result, err)
	}
}
func getById(service service.Service) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		result, err := service.GetById(ctx, ctx.Param("id"))
		response.NewResponse(ctx, result, err)
	}
}
func update(service service.Service) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var data entity.Save
		if err := ctx.BindJSON(&data); err != nil {
			response.NewResponse(ctx, nil, errors.New(code.INVALID_INPUT, "invalid requrest body", err))
			return
		}
		result, err := service.Update(ctx, ctx.Param("id"), data)
		response.NewResponse(ctx, result, err)
	}
}
func delete(service service.Service) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		result, err := service.Delete(ctx, ctx.Param("id"))
		response.NewResponse(ctx, result, err)
	}
}
