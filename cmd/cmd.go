package cmd

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gin-gonic/gin"
	"gitlab.com/mudiarta.tauda/go-starter/cmd/argument"
	"gitlab.com/mudiarta.tauda/go-starter/pkg/auth"
	todoDelivery "gitlab.com/mudiarta.tauda/go-starter/services/todo/delivery"
	todoRepo "gitlab.com/mudiarta.tauda/go-starter/services/todo/repository"
	todoService "gitlab.com/mudiarta.tauda/go-starter/services/todo/service"
)

func NewCommand() error {
	arg := argument.NewArgument()
	switch {
	case arg == argument.REST_API:
		return RestAPI()
	case arg == argument.TODO_WORKER_DELETE_ALL:
		return todoDelivery.NewWorkerDeleteAll()
	default:
		return fmt.Errorf("unknown command")
	}
}

func RestAPI() error {
	handler := gin.New()
	authorization := auth.NewAuthorization()

	// init repository
	todoRepo := todoRepo.NewRepository()

	//init handler
	todoDelivery.NewRestApi(handler, authorization, todoService.NewService(todoRepo))

	server := &http.Server{
		Handler: handler,
		Addr:    ":" + os.Getenv("PORT"),
	}

	go func() {
		fmt.Printf("Run App On %s\n", server.Addr)
		if err := server.ListenAndServe(); err != http.ErrServerClosed {
			fmt.Println(err.Error())
			return
		}
	}()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM, syscall.SIGQUIT)
	<-quit
	return ShutdownRestAPI(server)
}

func ShutdownRestAPI(server *http.Server) error {
	if err := server.Close(); err != nil {
		return err
	}
	return nil
}
