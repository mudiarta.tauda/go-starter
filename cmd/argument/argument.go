package argument

import (
	"os"
	"strings"
)

type Argument string

const (
	uNKNOWN                Argument = ""
	REST_API               Argument = "restapi"
	TODO_WORKER_DELETE_ALL Argument = "todo-worker-delete-all"
)

func NewArgument() Argument {
	args := os.Args
	for _, arg := range args {
		parts := strings.Split(arg, "=")
		if len(parts) == 2 && parts[0] == "cmd" {
			return Argument(parts[1])
		}
	}
	return uNKNOWN
}
