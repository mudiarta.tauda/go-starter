package main

import (
	"fmt"

	"gitlab.com/mudiarta.tauda/go-starter/cmd"
)

func main() {
	if err := cmd.NewCommand(); err != nil {
		fmt.Println(err.Error())
	}
}
