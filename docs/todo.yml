openapi: "3.0.2"
info:
  title: Todo API
  license:
      name: Apache 2.0
      url: http://www.apache.org/licenses/LICENSE-2.0.html
  version: 1.0.0
servers:
  - description: production
    url: https://api.todo.com
  - description: develop
    url: https://api.todo.net
  - description: local
    url: http://localhost:8080
paths:
  /todo/v1:
    get:
      summary: "[public]"
      description: "get todolist"
      parameters: 
      - name: search
        in: query
        schema:
          type: string
      responses:
        200:
          description: "todo list"
          content:
            application/json:
              schema:
                type: object
                properties:
                  code:
                    type: integer
                  message:
                    type: string
                  data:
                    type: array
                    items:
                      $ref: '#/components/schemas/Todo'
                      
                  errors:
                    type: string
                    default: null
        400:
          $ref: '#/components/responses/400'
        401:
          $ref: '#/components/responses/401'  
        404:
          $ref: '#/components/responses/404'
        500:
          $ref: '#/components/responses/500'
    post:
      summary: "[admin]"
      description: "create todo"
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Save'
      responses:
        200:
          description: "todo id"
          content:
            application/json:
              schema:
                type: object
                properties:
                  code:
                    type: integer
                  message:
                    type: string
                  data:
                    type: integer
                  errors:
                    type: string
                    default: null                                
        400:
          $ref: '#/components/responses/400'
        401:
          $ref: '#/components/responses/401'  
        404:
          $ref: '#/components/responses/404'
        500:
          $ref: '#/components/responses/500'        
  /todo/v1/{id}:
    get:
      summary: "[public]"
      description: "get todo by id"
      parameters:
        - $ref: '#/components/parameters/id'
      responses:
        200:
          description: "todo"
          content:
            application/json:
              schema:
                type: object
                properties:
                  code:
                    type: integer
                  message:
                    type: string
                  data:
                    $ref: '#/components/schemas/Todo'
                  errors:
                    type: string
                    default: null
        400:
          $ref: '#/components/responses/400'
        401:
          $ref: '#/components/responses/400'  
        404:
          $ref: '#/components/responses/404'
        500:
          $ref: '#/components/responses/500'
    put:
      summary: "[admin]"
      description: "update todo"
      parameters: 
      - $ref: '#/components/parameters/id'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Save'
      responses:
        200:
          description: "todo id"
          content:
            application/json:
              schema:
                type: object
                properties:
                  code:
                    type: integer
                  message:
                    type: string
                  data:
                    type: integer
                  errors:
                    type: string
                    default: null                               
        400:
          $ref: '#/components/responses/400'
        401:
          $ref: '#/components/responses/400'  
        404:
          $ref: '#/components/responses/404'
        500:
          $ref: '#/components/responses/500' 
    delete:
      summary: "[owner]"
      description: "delete todo"
      parameters: 
      - $ref: '#/components/parameters/id'
      responses:
        200:
          description: "todo id"
          content:
            application/json:
              schema:
                type: object
                properties:
                  code:
                    type: integer
                  message:
                    type: string
                  data:
                    type: integer
                  errors:
                    type: string
                    default: null                               
        400:
          $ref: '#/components/responses/400'
        401:
          $ref: '#/components/responses/400'  
        404:
          $ref: '#/components/responses/404'
        500:
          $ref: '#/components/responses/500' 
components:
  parameters:
    id:
      name: id
      in: path
      required: true
      schema:
        type: integer
  schemas:
    Todo:
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
        description:
          type: string
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
    Save:
      type: object
      properties:
        name:
          type: string
        description:
          type: string
  responses:
    400:
      description: "bad request"
      content:
        application/json:
          schema:
            type: object
            properties:
              code:
                type: integer
              message:
                type: string
              data:
                default: null
              errors:
                type: string
    401:
      description: "permission denied"
      content:
        application/json:
          schema:
            type: object
            properties:
              code:
                type: integer
              message:
                type: string
              data:
                default: null
              errors:
                type: string
    404:
      description: "not found"
      content:
        application/json:
          schema:
            type: object
            properties:
              code:
                type: integer
              message:
                type: string
              data:
                default: null
              errors:
                type: string
    500:
      description: "internal error"
      content:
        application/json:
          schema:
            type: object
            properties:
              code:
                type: integer
              message:
                type: string
              data:
                default: null
              errors:
                type: string            
  securitySchemes:
    authjwt:
      type: http
      scheme: bearer
      bearerFormat: JWT
security:
  - authjwt: []