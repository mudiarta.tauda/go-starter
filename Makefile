run:
	set -a && . ./.env.local && set +a  && \
	go build -o main.exe && ./main.exe cmd=$(cmd)
start-restapi:
	docker run -d --name restapi -p 8080:8080 --env-file $(PWD)/.env go-starter:v1.0.0 cmd=restapi
start-worker:
	docker run -d --name $(cmd) --env-file $(PWD)/.env go-starter:v1.0.0 cmd=$(cmd)
stop:
	docker stop $(cmd) && docker rm $(cmd)
